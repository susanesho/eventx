FactoryGirl.define do
  factory :manager_profile do
    subdomain "ladyb"
    company_name "Manogoa"
    company_mail "ema@a.com"
    company_phone "08062201524"
    domain "MyDomain"
  end
end
